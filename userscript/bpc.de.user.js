// ==UserScript==
// @name            Bypass Paywalls Clean - de/at/ch
// @version         3.1.1.8
// @downloadURL     https://gitlab.com/magnolia1234/bypass-paywalls-clean-filters/-/raw/main/userscript/bpc.de.user.js
// @updateURL       https://gitlab.com/magnolia1234/bypass-paywalls-clean-filters/-/raw/main/userscript/bpc.de.user.js
// @license         MIT; https://gitlab.com/magnolia1234/bypass-paywalls-clean-filters/-/blob/main/LICENSE
// @match           *://*.de/*
// @match           *://*.faz.net/*
// @match           *://*.kurier.at/*
// @match           *://*.nzz.ch/*
// @match           *://*.tagesanzeiger.ch/*
// @match           *://*.topagrar.at/*
// @match           *://*.topagrar.com/*
// @match           *://*.wochenblatt.com/*
// @match           *://webcache.googleusercontent.com/*
// ==/UserScript==

(function() {
  'use strict';

if (matchDomain('webcache.googleusercontent.com')) {
  window.setTimeout(function () {
    if (window.location.search.includes('q=cache:https://www.schwaebische.de')) {
      let ads = document.querySelectorAll('div.fp-ad-placeholder');
      hideDOMElement(...ads);
    }
  }, 1000);
}

window.setTimeout(function () {

var de_funke_medien_domains = ['abendblatt.de', 'braunschweiger-zeitung.de', 'morgenpost.de', 'nrz.de', 'otz.de', 'thueringer-allgemeine.de', 'tlz.de', 'waz.de', 'wp.de', 'wr.de'];
var de_lv_domains = ['profi.de', 'topagrar.at', 'topagrar.com', 'wochenblatt.com'];
var de_madsack_domains = ['haz.de', 'kn-online.de', 'ln-online.de', 'lvz.de', 'maz-online.de', 'neuepresse.de', 'ostsee-zeitung.de', 'rnd.de'];
var de_vrm_domains = ['allgemeine-zeitung.de', 'echo-online.de', 'wiesbadener-kurier.de'];
var de_westfalen_medien_domains = ['muensterschezeitung.de', 'westfalen-blatt.de', 'wn.de'];
var domain;

if (matchDomain('aerzteblatt.de')) {
  let paywall = document.querySelector('div#restrictedAccessLogin');
  if (paywall) {
    removeDOMElement(paywall);
    let restricted = document.querySelector('div.restricted');
    if (restricted)
      restricted.classList.remove('restricted');
    let unsharp = document.querySelector('div.unsharp');
    if (unsharp)
      unsharp.classList.remove('unsharp');
  }
}

else if (matchDomain('aerztezeitung.de')) {
  let paywall = document.querySelector('div.AZLoginModule');
  if (paywall) {
    removeDOMElement(paywall);
    let json_script = getArticleJsonScript();
    if (json_script) {
      let json = JSON.parse(json_script.text);
      if (json) {
        let json_text = json.articleBody;
        let content = document.querySelector('p.intro');
        if (json_text && content) {
          let article_new = document.createElement('p');
          article_new.innerText = json_text;
          content.after(article_new);
        }
      }
    }
  }
}

else if (matchDomain('allgaeuer-zeitung.de')) {
  let url = window.location.href;
  if (!url.includes('?type=amp')) {
    let paywall = document.querySelector('p.nfy-text-blur');
    if (paywall) {
      removeDOMElement(paywall);
      window.location.href = url.split('?')[0] + '?type=amp';
    }
  } else {
    let preview = document.querySelectorAll('p.nfy-text-blur, div[subscriptions-display^="NOT data."]');
    let amp_ads = document.querySelectorAll('amp-ad');
    removeDOMElement(...preview, ...amp_ads);
  }
}

else if (matchDomain('augsburger-allgemeine.de')) {
  let url = window.location.href;
  if (!url.includes('-amp.html')) {
    let paywall = document.querySelector('div.aa-visible-logged-out');
    if (paywall) {
      removeDOMElement(paywall);
      window.location.href = url.replace('.html', '-amp.html');
    }
  } else {
    amp_unhide_subscr_section();
  }
}

else if (matchDomain('cicero.de')) {
  let url = window.location.href;
  if (!window.location.search.match(/(\?|&)amp/)) {
    let paywall = document.querySelector('.plenigo-paywall');
    let amphtml = document.querySelector('link[rel="amphtml"]');
    if (paywall && amphtml) {
      removeDOMElement(paywall);
      window.location.href = amphtml.href;
    }
  } else {
    let teasered_content = document.querySelector('.teasered-content');
    if (teasered_content)
      teasered_content.classList.remove('teasered-content');
    let teasered_content_fader = document.querySelector('.teasered-content-fader');
    let btn_read_more = document.querySelector('.btn--read-more');
    let amp_ads = document.querySelectorAll('amp-ad');
    removeDOMElement(teasered_content_fader, btn_read_more, ...amp_ads);
  }
  let urban_ad_sign = document.querySelectorAll('.urban-ad-sign');
  removeDOMElement(...urban_ad_sign);
}

else if (matchDomain('faz.net')) {
  if (matchDomain('zeitung.faz.net')) {
    let paywall_z = document.querySelector('.c-red-carpet');
    if (paywall_z) {
      removeDOMElement(paywall_z);
      let og_url = document.querySelector('meta[property="og:url"][content]');
      if (og_url)
        window.location.href = og_url.content;
    }
    let sticky_advt = document.querySelector('.sticky-advt');
    removeDOMElement(sticky_advt);
  } else {
    let paywall = document.querySelector('#paywall-form-container-outer, .atc-ContainerPaywall');
    if (paywall) {
      removeDOMElement(paywall);
      let json_script = getArticleJsonScript();
      if (json_script) {
        let json_text = JSON.parse(json_script.text).articleBody;
        if (json_text) {
          let article_text = document.querySelector('.art_txt.paywall,.atc-Text.js-atc-Text');
          article_text.innerText = '';
          let elem = document.createElement("p");
          elem.setAttribute('class', 'atc-TextParagraph');
          elem.innerText = json_text;
          article_text.appendChild(elem);
        }
      }
    }
    let lay_paysocial = document.querySelector('div.lay-PaySocial');
    let ads = document.querySelectorAll('div.iqadtile_wrapper');
    removeDOMElement(lay_paysocial, ...ads);
  }
}

else if (matchDomain('freiepresse.de')) {
  let url = window.location.href;
  let article_teaser = document.querySelector('div.article-teaser');
  if (article_teaser && url.match(/(\-artikel)(\d){6,}/)) {
    window.setTimeout(function () {
      window.location.href = url.replace('-artikel', '-amp');
    }, 500);
  } else if (url.match(/(\-amp)(\d){6,}/)) {
    let amp_ads = document.querySelectorAll('amp-fx-flying-carpet, amp-ad, amp-embed');
    let pw_layer = document.querySelector('.pw-layer');
    removeDOMElement(...amp_ads, pw_layer);
  }
}

else if (matchDomain('krautreporter.de')) {
  let paywall = document.querySelector('.js-article-paywall');
  if (paywall) {
    removeDOMElement(paywall);
    window.setTimeout(function () {
      let paywall_divider = document.querySelector('.js-paywall-divider');
      let steady_checkout = document.querySelector('#steady-checkout');
      removeDOMElement(paywall_divider, steady_checkout);
      let blurred = document.querySelectorAll('.blurred');
      for (let elem of blurred)
        elem.classList.remove('blurred', 'json-ld-paywall-marker', 'hidden@print');
    }, 500);
  }
}

else if (matchDomain(['ksta.de', 'rundschau-online.de'])) {
  let paywall = document.querySelector('div[data-tm-placeholder]');
  if (paywall) {
    removeDOMElement(paywall);
    let span_hidden = document.querySelector('div.dm-paint');
    if (span_hidden)
      span_hidden.removeAttribute('class');
  } else {
    let paywall_new = document.querySelector('div.paywalled-content');
    if (paywall_new)
      paywall_new.removeAttribute('class');
    let wrapper = document.querySelector('div.dm-paywall-wrapper');
    removeDOMElement(wrapper);
  }
  let banners = document.querySelectorAll('div.dm-slot, div[id^="taboola-feed"]');
  removeDOMElement(...banners);
}

else if (matchDomain('kurier.at')) {
  let paywall = document.querySelector('div.plusContent');
  if (paywall) {
    paywall.classList.remove('plusContent');
    window.setTimeout(function () {
      let elem_hidden = paywall.querySelectorAll('.ng-star-inserted[style="display: none;"]');
      for (let elem of elem_hidden)
        elem.removeAttribute('style');
    }, 2000);
  }
  let banners = document.querySelectorAll('div#view-offer, app-paywall, adfullbanner, outbrain');
  removeDOMElement(...banners);
}

else if (matchDomain(['mz.de', 'volksstimme.de'])) {
  let url = window.location.href.split('?')[0];
  let paywall = document.querySelector('.fp-paywall');
  if (url.includes('/amp/')) {
    amp_unhide_subscr_section('amp-ad, amp-embed');
  } else {
    if (paywall) {
      removeDOMElement(paywall);
      window.location.href = window.location.href.replace('.de/', '.de/amp/');
    }
  }
}

else if (matchDomain(['shz.de', 'svz.de'])) {
  if (window.location.pathname.endsWith('/amp')) {
    amp_unhide_access_hide('="NOT data.reduced"', '="data.reduced"', 'amp-ad, amp-embed, .ads-wrapper, #flying-carpet-wrapper');
  } else {
    let paywall = document.querySelector('.paywall');
    let amphtml = document.querySelector('link[rel="amphtml"]');
    if (paywall && amphtml) {
      removeDOMElement(paywall);
      window.location.href = amphtml.href;
    } else {
      let ads = document.querySelectorAll('div.nozmhn_ad');
      hideDOMElement(...ads);
    }
  }
}

else if (matchDomain('nw.de')) {
  if (!window.location.pathname.endsWith('.amp.html')) {
    let paywall = document.querySelector('div[data-tracking-visible^="paywall-"]');
    let amphtml = document.querySelector('link[rel="amphtml"]');
    if (paywall && amphtml) {
      removeDOMElement(paywall);
      window.location.href = amphtml.href;
    }
  } else {
    amp_unhide_access_hide('="loggedIn AND hasAbo"', '', 'amp-ad, amp-embed, . banner');
  }
}

else if (matchDomain('nwzonline.de')) {
  if (window.location.pathname.match(/-amp\.html$/)) {
    amp_unhide_access_hide('="NOT data.reduced"', '="data.reduced"', 'amp-ad, amp-embed');
  } else {
    let paywall = document.querySelector('.story--premium__container, .paywall-overlay');
    if (paywall) {
      removeDOMElement(paywall);
      window.location.href = window.location.pathname.replace('.html', '-amp.html');
    }
  }
}

else if (matchDomain('nzz.ch')) {
  if (!window.location.href.includes('/amp/')) {
    let paywall = document.querySelector('.dynamic-regwall');
    let amphtml = document.querySelector('link[rel="amphtml"]');
    if (paywall && amphtml) {
      removeDOMElement(paywall);
      window.location.href = amphtml.href;
    } else {
      let ads = document.querySelectorAll('div.resor');
      hideDOMElement(...ads);
    }
  } else {
    let amp_ads = document.querySelectorAll('amp-ad');
    removeDOMElement(...amp_ads);
  }
}

else if (matchDomain('philomag.de')) {
  let paywall = document.querySelector('div[id^="block-paywall"]');
  if (paywall) {
    removeDOMElement(paywall);
    let json_script = getArticleJsonScript();
    if (json_script) {
      let json = JSON.parse(json_script.text);
      if (json) {
        let json_text = json.articlebody.replace(/%paywall%/g, '').replace(/(\\r)?\\n/g, '<br><br>');
        let content = document.querySelector('div.content-center > div.description');
        if (json_text && content) {
          content.innerHTML = '';
          let article_new = document.createElement('p');
          article_new.innerText = json_text;
          content.appendChild(article_new);
        }
      }
    }
  }
}

else if (matchDomain('schwaebische.de')) {
  let url = window.location.href;
  let paywall = document.querySelector('div.sve-paywall-wrapper');
  if (paywall) {
    removeDOMElement(paywall);
    let article = document.querySelector('div.article_body');
    if (article)
      article.firstChild.before(googleWebcacheLink(url));
    let body = document.querySelector('body');
    if (body)
      body.removeAttribute('style');
    waitDOMAttribute('body', 'body', 'style', node => node.removeAttribute('style'), true);
  }
  let ads = document.querySelectorAll('div.fp-ad-placeholder');
  hideDOMElement(...ads);
}

else if (matchDomain('spiegel.de')) {
  let url = window.location.href;
  let paywall = document.querySelector('div[data-area="paywall"]');
  if (paywall) {
    removeDOMElement(paywall);
    let article = document.querySelector('div[data-area="body"]');
    if (article)
      article.insertBefore(archiveLink(url), article.firstChild);
  }
}

else if (matchDomain('sueddeutsche.de')) {
  let url = window.location.href;
  let paywall = document.querySelector('offer-page, div.offer-page');
  if (paywall) {
    removeDOMElement(paywall);
    let article = document.querySelector('article, main > section > div > p');
    if (article)
      article.firstChild.before(archiveLink(url));
  }
}

else if (matchDomain('tagesanzeiger.ch')) {
  let url = window.location.href;
  let paywall = document.querySelector('div#piano-premium > div');
  if (paywall) {
    removeDOMElement(paywall.parentNode);
    let article = document.querySelector('article p');
    if (article)
      article.firstChild.before(archiveLink(url));
  }
}

else if (matchDomain('tagesspiegel.de')) {
  let url = window.location.href;
  let paywall = document.querySelector('div.article--paid > div');
  if (paywall) {
    removeDOMElement(paywall);
    let article = document.querySelector('div.article--paid');
    if (article)
      article.firstChild.before(archiveLink(url));
  }
}

else if (matchDomain('zeit.de')) {
  let url = window.location.href;
  let paywall = document.querySelector('#paywall');
  if (paywall) {
    removeDOMElement(paywall);
    let article = document.querySelector('div.article-body');
    if (article)
      article.firstChild.before(ext_12ftLink(url));
    let fade = document.querySelector('div.paragraph--faded');
    if (fade)
      fade.classList.remove('paragraph--faded');
  }
}

else if (matchDomain(de_lv_domains)) {
  let paywall_topagrar = document.querySelector('div > div.paywall-package');
  let paywall_other = document.querySelector('div[id^="paymentprocess-"]');
  if (paywall_topagrar || paywall_other) {
    if (paywall_topagrar)
      removeDOMElement(paywall_topagrar.parentNode);
    else {
      let intro = document.querySelector('div.m-paywall__textFadeOut');
      removeDOMElement(paywall_other, intro);
    }
    let div_hidden = document.querySelector('div.paywall-full-content[style]');
    if (div_hidden) {
      div_hidden.removeAttribute('class');
      div_hidden.removeAttribute('style');
    }
  }
  let banners = document.querySelectorAll('.adZone');
  removeDOMElement(...banners);
}

else if (matchDomain(de_westfalen_medien_domains)) {
  let url = window.location.href;
  if (url.includes('/amp/')) {
    amp_unhide_subscr_section('amp-ad, amp-embed, section[class^="fp-ad"]');
  } else {
    let paywall = document.querySelector('.fp-article-paywall');
    let amphtml = document.querySelector('link[rel="amphtml"]');
    if (paywall) {
      removeDOMElement(paywall);
      if (amphtml)
        window.location.href = amphtml.href;
    }
  }
}

else if (matchDomain(de_funke_medien_domains) || document.querySelector('a[href="https://www.funkemedien.de/"]')) {
  if (window.location.search.startsWith('?service=amp'))
    amp_unhide_access_hide('="NOT p.showRegWall AND NOT p.showPayWall"', '', 'amp-ad, amp-embed, amp-fx-flying-carpet');
  else
    sessionStorage.setItem('deobfuscate', 'true');
}

else if (matchDomain(de_madsack_domains) || document.querySelector('link[href*=".rndtech.de/"]')) {
  if (!window.location.search.startsWith('?outputType=valid_amp')) {
    let ads = document.querySelectorAll('div[class^="Adstyled__AdWrapper"]');
    hideDOMElement(...ads);
  } else {
    ampToHtml();
  }
}

else if (matchDomain(de_vrm_domains) || document.querySelector('meta[name^="cXenseParse:vrm-"]')) {
  let url = window.location.href;
  let paywall = document.querySelector('div.storyElementWrapper__paywallContainer');
  let article = document.querySelector('section[data-testid="storyPage-main-content"]');
  if (paywall && article) {
    removeDOMElement(paywall);
    fetch(url)
    .then(response => {
      if (response.ok) {
        response.text().then(html => {
          if (html.includes('window.__INITIAL_STATE__=')) {
            let split1 = html.split('window.__INITIAL_STATE__=')[1];
            let state = (split1.split('};')[0] + '}').split('</script>')[0];
            if (state) {
              try {
                let json = JSON.parse(state);
                if (json) {
                  let json_text = json.contentPage.data.context.storylineText;
                  let pars = json_text.split(/(?=<p>)/);
                  let page_items = json.contentPage.data.context.elements;
                  if (json_text && page_items) {
                    let parser = new DOMParser();
                    let n = 0;
                    let new_article = document.createElement('div');
                    new_article.setAttribute('style', 'font-size: 17px; font-family:Calibri, Helvetica, Arial; margin: auto; max-width: 620px;');
                    for (let item of page_items) {
                      let elem = document.createElement('p');
                      if (item.type === 'paragraph') {
                        if (pars[n]) {
                          let par = pars[n];
                          n++;
                          elem.innerText = par.replace(/<\/?p>/g, '');
                        }
                      } else if (item.type === 'subhead') {
                        if (item.fields) {
                          elem = document.createElement('h2');
                          elem.appendChild(document.createTextNode(item.fields[0].value));
                        }
                      } else if (item.type === 'internal_link') {
                        if (item.relation) {
                          let par_link = document.createElement('a');
                          par_link.href = item.relation.href;
                          par_link.innerText = item.relation.title;
                          elem.appendChild(par_link);
                        }
                      } else if (item.type === 'embed') {
                        let fields = item.fields[0];
                        if (fields.name && fields.name === 'uri' && fields.value) {
                          let par_link = document.createElement('a');
                          par_link.href = fields.value;
                          par_link.innerText = fields.value.split('?')[0];
                          par_link.target = '_blank';
                          elem.appendChild(par_link);
                        }
                      } else if (!['dateline', 'headline', 'image', 'leadtext', 'linkbox', 'linkbox_title', 'relation'].includes(item.type)) {
                        elem = '';
                        console.log(item.type);
                        console.log(item);
                      }
                      if (elem)
                        new_article.appendChild(elem);
                    }
                      if (new_article.hasChildNodes())
                        article.appendChild(new_article);
                  }
                }
              } catch (err) {
                console.log(err);
              }
            }
          }
        })
      }
    })
  }
}

}, 1000);

// General Functions

function matchDomain(domains, hostname) {
  var matched_domain = false;
  if (!hostname)
    hostname = window.location.hostname;
  if (typeof domains === 'string')
    domains = [domains];
  domains.some(domain => (hostname === domain || hostname.endsWith('.' + domain)) && (matched_domain = domain));
  return matched_domain;
}

function setCookie(name, value, domain, path, days) {
  window.localStorage.clear();
  var max_age = days * 24 * 60 * 60;
  document.cookie = name + "=" + (value || "") + "; domain=" + domain + "; path=" + path + "; max-age=" + max_age;
}

function removeDOMElement(...elements) {
  for (let element of elements) {
    if (element)
      element.remove();
  }
}

function hideDOMElement(...elements) {
  for (let element of elements) {
    if (element)
      element.style = 'display:none;';
  }
}

function waitDOMElement(selector, tagName = '', callback, multiple = false) {
  new window.MutationObserver(function (mutations) {
    for (let mutation of mutations) {
      for (let node of mutation.addedNodes) {
        if (!tagName || (node.tagName === tagName)) {
          if (node.matches(selector)) {
            callback(node);
            if (!multiple)
              this.disconnect();
          }
        }
      }
    }
  }).observe(document, {
    subtree: true,
    childList: true
  });
}

function waitDOMAttribute(selector, tagName = '', attributeName = '', callback, multiple = false) {
  let targetNode = document.querySelector(selector);
  if (!targetNode)
    return;
  new window.MutationObserver(function (mutations) {
    for (let mutation of mutations) {
      if (mutation.target.attributes[attributeName]) {
        callback(mutation.target);
        if (!multiple)
          this.disconnect();
      }
    }
  }).observe(targetNode, {
    attributes: true,
    attributeFilter: [attributeName]
  });
}

function amp_iframes_replace(weblink = false, source = '') {
  let amp_iframes = document.querySelectorAll('amp-iframe' + (source ? '[src*="'+ source + '"]' : ''));
  let par, elem;
  for (let amp_iframe of amp_iframes) {
    if (!weblink) {
      elem = document.createElement('iframe');
      Object.assign(elem, {
        src: amp_iframe.getAttribute('src'),
        sandbox: amp_iframe.getAttribute('sandbox'),
        height: amp_iframe.getAttribute('height'),
        width: 'auto',
        style: 'border: 0px;'
      });
      amp_iframe.parentNode.replaceChild(elem, amp_iframe);
    } else {
      par = document.createElement('p');
      elem = document.createElement('a');
      elem.innerText = 'Media-link';
      elem.setAttribute('href', amp_iframe.getAttribute('src'));
      elem.setAttribute('target', '_blank');
      par.appendChild(elem);
      amp_iframe.parentNode.replaceChild(par, amp_iframe);
    }
  }
}

function amp_unhide_subscr_section(amp_ads_sel = 'amp-ad, .ad', replace_iframes = true, amp_iframe_link = false, source = '') {
  let preview = document.querySelectorAll('[subscriptions-section="content-not-granted"]');
  removeDOMElement(...preview);
  let subscr_section = document.querySelectorAll('[subscriptions-section="content"]');
  for (let elem of subscr_section)
    elem.removeAttribute('subscriptions-section');
  let amp_ads = document.querySelectorAll(amp_ads_sel);
  removeDOMElement(...amp_ads);
  if (replace_iframes)
    amp_iframes_replace(amp_iframe_link, source);
}

function amp_unhide_access_hide(amp_access = '', amp_access_not = '', amp_ads_sel = 'amp-ad, .ad', replace_iframes = true, amp_iframe_link = false, source = '') {
  let access_hide = document.querySelectorAll('[amp-access' + amp_access + '][amp-access-hide]:not([amp-access="error"], [amp-access^="message"])');
  for (let elem of access_hide)
    elem.removeAttribute('amp-access-hide');
  if (amp_access_not) {
    let amp_access_not_dom = document.querySelectorAll('[amp-access' + amp_access_not + ']');
    removeDOMElement(...amp_access_not_dom);
  }
  let amp_ads = document.querySelectorAll(amp_ads_sel);
  removeDOMElement(...amp_ads);
  if (replace_iframes)
    amp_iframes_replace(amp_iframe_link, source);
}

function ampToHtml() {
  window.setTimeout(function () {
    let canonical = document.querySelector('link[rel="canonical"]');
    window.location.href = canonical.href;
  }, 500);
}

function archiveLink(url, text_fail = 'BPC > Full article text (only report issue if not working for over a week):\r\n') {
  return externalLink(['archive.today', 'archive.is'], 'https://{domain}?run=1&url={url}', url, text_fail);
}

function googleWebcacheLink(url, text_fail = 'BPC > Full article text:\r\n') {
  return externalLink(['webcache.googleusercontent.com'], 'https://{domain}/search?q=cache:{url}', url, text_fail);
}

function ext_12ftLink(url, text_fail = 'BPC > Full article text:\r\n') {
  return externalLink(['12ft.io'], 'https://{domain}/{url}', url, text_fail);
}

function externalLink(domains, ext_url_templ, url, text_fail = 'BPC > Full article text:\r\n') {
  let text_fail_div = document.createElement('div');
  text_fail_div.id = 'bpc_archive';
  text_fail_div.setAttribute('style', 'margin: 20px; font-weight: bold; color:red;');
  text_fail_div.appendChild(document.createTextNode(text_fail));
  for (let domain of domains) {
    let ext_url = ext_url_templ.replace('{domain}', domain).replace('{url}', url.split('?')[0]);
    let a_link = document.createElement('a');
    a_link.innerText = domain;
    a_link.href = ext_url;
    a_link.target = '_blank';
    text_fail_div.appendChild(document.createTextNode(' | '));
    text_fail_div.appendChild(a_link);
  }
  return text_fail_div;
}

function parseHtmlEntities(encodedString) {
  let translate_re = /&(nbsp|amp|quot|lt|gt|deg|hellip|laquo|raquo|ldquo|rdquo|lsquo|rsquo|mdash);/g;
  let translate = {"nbsp": " ", "amp": "&", "quot": "\"", "lt": "<", "gt": ">", "deg": "°", "hellip": "…",
      "laquo": "«", "raquo": "»", "ldquo": "“", "rdquo": "”", "lsquo": "‘", "rsquo": "’", "mdash": "—"};
  return encodedString.replace(translate_re, function (match, entity) {
      return translate[entity];
  }).replace(/&#(\d+);/gi, function (match, numStr) {
      let num = parseInt(numStr, 10);
      return String.fromCharCode(num);
  });
}

function encode_utf8(str) {
  return unescape(encodeURIComponent(str));
}

function decode_utf8(str) {
  return decodeURIComponent(escape(str));
}

function getArticleJsonScript() {
  let scripts = document.querySelectorAll('script[type="application/ld+json"]');
  let json_script;
  for (let script of scripts) {
    if (script.innerText.match(/"(articlebody|text)":/i)) {
      json_script = script;
      break;
    }
  }
  return json_script;
}

})();
